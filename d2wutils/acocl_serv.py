#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Remote service simulator

A simple web server to simulate remote services during WordPess
plugin development. Customise responses as needed by the project
by creating a subclass.

Usage:
    ./pyRemoteServ.py [<port>]
    

Author:     Anthony Lopez-Vito, Another Cup of Coffee Limited
Contact:    http://anothercoffee.net
Version:    v0.3
Date:       2019-11-14

License:
Unless otherwise specified by the project requirements, this code
has been released under the MIT License (MIT)
Copyright (c) 2018 Another Cup of Coffee Limited

See LICENSE.txt 
"""
# Enable as needed for project
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse
from urllib.parse import parse_qs
import json
import logging, logging.handlers
from collections import namedtuple
from datetime import datetime
from d2wutils.utils import setup_logging
from d2wutils.utils import get_settings
#from MySQLdb import OperationalError
#import subprocess
#import MySQLdb as mdb
#from prettytable import PrettyTable
import os, sys, subprocess
#from phpserialize import unserialize
#import subprocess
# Ensures cursors are closed upon completion of with block
# See discussion at http://stackoverflow.com/questions/5669878/python-mysqldb-when-to-close-cursors
#from contextlib import closing
"""
***** Raising exceptions on warnings *****
* Migration stages often rely on a the successful completion of a 
* previous stage. Warnings may mean that data was not correctly
* prepared for a subsequent stage. In most cases, you want to
* raise an exception on warnings so you can investigate the problem.

error: turn matching warnings into exceptions
ignore: never print matching warnings
always: always print matching warnings
default: print the first occurrence of matching warnings for each location where the warning is issued
module: print the first occurrence of matching warnings for each module where the warning is issued
once: print only the first occurrence of matching warnings, regardless of location

See:
https://docs.python.org/2/library/warnings.html#warning-filter
"""
#from warnings import filterwarnings
# Uncomment to suppress all warnings
#filterwarnings('ignore', category = mdb.Warning)
# Uncomment to raise exceptions on warnings
#filterwarnings('error', category = mdb.Warning)

if sys.version_info[0] < 3:
    raise Exception("You need Python 3 or higher to run this script.")


_logger = logging.getLogger(__name__) #logging.getLogger()
_dbconn = None
_webroot = None
_debug = False


##################################################################
# Constants
##################################################################
HTTP_STATUS_MSG_OK                      = "OK"                      #code=200,
HTTP_STATUS_MSG_BAD_REQUEST             = "Bad request"             #code=400
HTTP_STATUS_MSG_NOT_FOUND               = "Not found"               #code=404
HTTP_STATUS_MSG_INTERNAL_SERVER_ERROR   = "Internal server error"   #code=500

DEFAULT_HOST = "localhost"
DEFAULT_PORT = 8000

PROTOCOL = "http"
PAGE_INDEX = "/index.html"


##################################################################
# Make sure you edit these globals
##################################################################
FILE_SAMPLE_JSON = "/sample.json"


##################################################################
# Classes
##################################################################
class Serv(BaseHTTPRequestHandler):
    """A simple HTTP server
    """

    def do_HEAD(self):
        """
        """
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()


    def do_GET(self):
        """
        """
        # Extract path and values from the query string
        path, query_component, query_string = self.path.partition('?')
        query = parse_qs(query_string)
        logging.info("*** GET request ***");

        if path == "/":
            path = PAGE_INDEX

        try:
            self.get_request_handler(path)
        except IOError:
            self.send_error(
                404,
                HTTP_STATUS_MSG_NOT_FOUND,
                "Could not open file {}".format(path)
            )
        except Exception as ex:
            template = "A {0} exception occured:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            _logger.error(message)
            self.send_error(500, HTTP_STATUS_MSG_INTERNAL_SERVER_ERROR, message)

        logging.info(
            "GET request data: \nPath: %s\nHeaders:\n%s\n",
            str(self.path),
            str(self.headers)
        )
        return


    def do_POST(self):
        """
        """
        post_data = None
        logging.info("*** POST request ***");
        try:
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length)
            self.post_request_handler(post_data)
        except TypeError:
            _logger.info("TypeError on headers Content-Length. Probably no content.")

        logging.info(
            "POST request data \nPath: %s\nHeaders:\n%s",
            str(self.path),
            str(self.headers)
        )
        if(post_data):
            logging.info("Body:\n%s\n", post_data.decode('utf-8'))
        return


    def send_json(self, json_dict):
        """
        """
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        #self.wfile.write(json.dumps(content).encode())
        self.wfile.write(json.dumps(json_dict).encode())


    def send_html(self, content):
        """
        """
        if(isinstance(content, str)):
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(content.encode(encoding = 'utf_8'))
        elif(isinstance(content, bytes)):
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(content)
        else:
            message = "Don't know how to handle content type: {}".format(
                type(content)
            )
            _logger.error(
                message,
                type(content)
            )
            self.send_error(
                500,
                HTTP_STATUS_MSG_INTERNAL_SERVER_ERROR,
                message,
            )


    def read_json_file(self, filename):
        """Get data from external json file
        json file needs to be opened with mode 'r'
        
        Args:
            filename (string): file relative to the script directory

        Returns:
            results: contents of the file
        """
        filepath = sys.path[0]
        try:
            filename = os.path.join(filepath, filename[1:])
            with open(filename, "r") as read_file:
                content = json.load(read_file)
        except IOError:
            _logger.error("Could not open file {}".format(filename))
            raise
        return content


    def read_page_file(self, filename):
        """Get HTML page content from a file
        HTML file needs to be opened with mode 'rb'

        Args:
            filename (string): file relative to the script directory

        Returns:
            results: contents of the file
        """
        filepath = sys.path[0]
        
        try:
            filename = os.path.join(filepath, filename[1:])
            with open(filename, "rb") as read_file:
                content = read_file.read()
        except Exception as ex:
            template = "A {0} exception occured:\n{1!r}"
            message = template.format(type(ex).__name__, ex.args)
            _logger.error(message)
                
        """
        except IOError as err:
            self._logger.error("Could not open file {}".format(filename))
            raise
        """
        return content


    ##################################################################
    # Modify for the project or override these methods in the subclass
    ##################################################################
    def get_request_handler(self, path):
        """Handles GET requests. Customise this for the project
        """
        _logger.info("get_request_handler()")
        if path == PAGE_INDEX:
            content = self.read_page_file(PAGE_INDEX)
            self.send_html(content)
        elif self.path.endswith("/helloworld"):
            self.send_html("<html><body>Hello!</body></html>")
        elif self.path.endswith(".json"):
            json_data = self.read_json_file(FILE_SAMPLE_JSON)
            self.send_json(json_data)
        else:
            self.send_error(
                404,
                HTTP_STATUS_MSG_NOT_FOUND,
                "Could not handle request for {}".format(path[1:]
                )
            )


    def post_request_handler(self, post_data):
        """Handles POST requests. Customise this for the project
        """
        _logger.info("post_request_handler()")
        data = json.loads(post_data.decode('utf-8'))
        # Some sample json
        #parsed_path = urlparse(self.path)
        #self.wfile.write(json.dumps({
        #    'method': self.command,
        #    'path': self.path,
        #    'real_path': parsed_path.query,
        #    'query': parsed_path.query,
        #    'request_version': self.request_version,
        #    'protocol_version': self.protocol_version,
        #    'body': data
        #}).encode())
        self.send_json(data)
