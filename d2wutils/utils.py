#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""ACoCL project utilities.

Utility functions and procedures for ACoCL projects.
Edited for use with OIDCSimulator, February 2021.


Author:     Anthony Lopez-Vito, Another Cup of Coffee Limited
Contact:    http://anothercoffee.net

License:
Unless otherwise specified by the project requirements, this code
has been released under the MIT License (MIT)
Copyright (c) 2017 Another Cup of Coffee Limited

See LICENSE.txt 
"""

import sys, getopt, os
import logging, logging.handlers
import yaml
from datetime import datetime
import json

##################################################################
# Utility functions
##################################################################

def setup_logging(logger, settings, logging_level=logging.INFO):
    """Log output

        Sends log output to console or file,
        depending on error level
    """
    try:
        log_filename = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            settings['log_filename']
        )
        log_max_bytes = settings['log_max_bytes']
        log_backup_count = settings['log_backup_count']
    except KeyError as ex: 
        print("WARNING: Missing logfile setting {}. Using defaults.".format(ex))
        log_filename = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "log.txt"
        )
        log_max_bytes = 1048576 #1MB
        log_backup_count = 5

    logger.setLevel(logging.DEBUG)
    # Set up logging to file
    file_handler = logging.handlers.RotatingFileHandler(
        filename=log_filename,
        maxBytes=log_max_bytes,
        backupCount=log_backup_count
    )
    file_handler.setLevel(logging.DEBUG)
    file_formatter = logging.Formatter(
        '%(asctime)s %(name)-15s %(levelname)-8s %(message)s',
        '%m-%d %H:%M'
    )
    file_handler.setFormatter(file_formatter)
    logger.addHandler(file_handler)

    # Handler to write INFO messages or higher to sys.stderr
    #
    # SET LOGGING LEVEL HERE
    # * INFO
    # * DEBUG
    # * ERROR
    # See: https://docs.python.org/2/library/logging.html#logging-levels
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging_level) # <--- Set logging level
    #console_handler.setLevel(logging.INFO) # <--- Set logging level
    #console_handler.setLevel(logging.DEBUG) # <--- Set logging level
    console_formatter = logging.Formatter('%(levelname)-8s %(message)s')
    console_handler.setFormatter(console_formatter)
    logger.addHandler(console_handler)
    logger.debug("---------------------------------")
    logger.debug(
        "Starting log for session %s",
        datetime.now().strftime("%d/%m/%Y %H:%M:%S:%f")
    )


def get_settings(settings_file):
    """Get settings from external YAML file
    """
    settings = {}
    try:
        with open(settings_file, 'r') as ymlfile:
            settings = yaml.load(ymlfile)
    except IOError:
        print("Could not open settings file {}".format(settings_file))
        raise
    return settings


def print_progress_bar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    
    See: http://stackoverflow.com/a/34325723/1377876
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '░' * (length - filledLength)
    sys.stdout.write('\r')
    sys.stdout.write('%s |%s| %s%% %s\r' % (prefix, bar, percent, suffix))
    sys.stdout.flush()
    # Print New Line on Complete
    if iteration == total:
        sys.stdout.write('\n')


def exit_on_exception(ex, exit_code):
    """Exit the script on exception

    Arguments:
        ex:         Exception
        exit_code:  Exit code integer 0, 1 or 2
    
    Unix programs generally use 2 for command line syntax errors
    and 1 for all other kind of errors.
    https://docs.python.org/2/library/sys.html#sys.exit
    """
    template = "A {0} exception occured:\n{1!r}"
    message = template.format(type(ex).__name__, ex.args)
    print(message)
    sys.exit(exit_code)

