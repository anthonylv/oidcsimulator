# OIDCSimulator

A simple Python-based JSON HTTP server to simulate OpenID Connect (OIDC) endpoints.

OIDCSimulator is a very simple HTTP server written in Python that will accept POST or GET requests with accompanying JSON data and reply with fixed JSON responses.

When building an OIDC plugin for a client, it is often not possible to interact with the OIDC server endpoints from a local development environment. However, it will be necessary to transmit data over the network during the development and testing. This is my in-house utility for local development testing to simulate OIDC endpoints when building a custom OIDC plugin.

It enables us to:

1. inspect the requests being sent by the plugin to the endpoint;
2. send a preset and known response for the plugin to handle.

OIDCSimulator uses template code from Another Cup of Coffee Limited's in-house utility, pyRemoteServ, for WordPress development projects. It has been minimally customised for OIDC plugin projects.

## Usage

### JSON Response
You will need to create a simulated JSON response which can be placed in the file `sample.json.` The content of this file will depend on your project

### Run the server:
`./pyRemoteServ.py [<port>]`

POST and GET requests along with the accompanying JSON data will be displayed in the terminal.

### Connecting to the endpoint:
The server will run at http://127.0.0.1:8000 by default. To test the OIDC plugin, set it to connect to the endpoint. For example: http://127.0.0.1:8000/wordpress/users/purchase/. The server will respond to requests from a hardcoded user ID and service secret.

## Customising the JSON reponse

To customise responses, edit post_request_handler() and get_request_handler() in the `pyRemoteServ.py`
The hard-coded user ID and service secret is set in the global variables `TEST_OIDC_USER_ID` and `TEST_OIDC_SERVICE_SECRET`.


## License
Written by Anthony Lopez-Vito of [Another Cup of Coffee Limited](http://anothercoffee.net). All code is released under The MIT License.
Please see LICENSE.txt.
