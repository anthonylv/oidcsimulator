#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""OIDC Endpoint Simulator

A simple web server to simulate OIDC endpoints for a custom OIDC plugin
project.

Using template code from pyRemoteServ v0.3 by Another Cup of Coffee Limited.

Usage:
    ./pyRemoteServ.py [<port>]
    

Author:     Anthony Lopez-Vito, Another Cup of Coffee Limited
Contact:    http://anothercoffee.net
Version:    From pyRemoteServ v0.3 code template
Date:       Modified for this project 2021-02-21

License:
Unless otherwise specified by the project requirements, this code
has been released under the MIT License (MIT)
Copyright (c) 2018 Another Cup of Coffee Limited

See LICENSE.txt 
"""
# Enable as needed for project
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse
from urllib.parse import parse_qs
import json
import logging, logging.handlers
from datetime import datetime
from d2wutils.utils import setup_logging
from d2wutils.utils import get_settings
#from MySQLdb import OperationalError
#import subprocess
#import MySQLdb as mdb
#from prettytable import PrettyTable
import os, sys, subprocess
#from phpserialize import unserialize
#import subprocess
# Ensures cursors are closed upon completion of with block
# See discussion at http://stackoverflow.com/questions/5669878/python-mysqldb-when-to-close-cursors
#from contextlib import closing
"""
***** Raising exceptions on warnings *****
* Migration stages often rely on a the successful completion of a 
* previous stage. Warnings may mean that data was not correctly
* prepared for a subsequent stage. In most cases, you want to
* raise an exception on warnings so you can investigate the problem.

error: turn matching warnings into exceptions
ignore: never print matching warnings
always: always print matching warnings
default: print the first occurrence of matching warnings for each location where the warning is issued
module: print the first occurrence of matching warnings for each module where the warning is issued
once: print only the first occurrence of matching warnings, regardless of location

See:
https://docs.python.org/2/library/warnings.html#warning-filter
"""
#from warnings import filterwarnings
# Uncomment to suppress all warnings
#filterwarnings('ignore', category = mdb.Warning)
# Uncomment to raise exceptions on warnings
#filterwarnings('error', category = mdb.Warning)

if sys.version_info[0] < 3:
    raise Exception("You need Python 3 or higher to run this script.")

_logger = logging.getLogger()
_dbconn = None
_webroot = None
_debug = False

##################################################################
# Constants
##################################################################
HTTP_STATUS_MSG_OK                      = "OK"                      #code=200,
HTTP_STATUS_MSG_BAD_REQUEST             = "Bad request"             #code=400
HTTP_STATUS_MSG_NOT_FOUND               = "Not found"               #code=404
HTTP_STATUS_MSG_INTERNAL_SERVER_ERROR   = "Internal server error"   #code=500

DEFAULT_HOST = "localhost"
DEFAULT_PORT = 8000

PROTOCOL = "http"
PAGE_INDEX = "/index.html"


##################################################################
# Make sure you edit these globals
##################################################################
JSON_SERVICE_ERROR = {
    "status":"failed",
    "message":"Error: service or service key invalid"
}

JSON_SERVICE_OK = {
    "status":"success"
}

FILE_SAMPLE_JSON = "/sample.json"
ENDPOINT_PURCHASE_GET = "/wordpress/users/purchase/"

TEST_OIDC_USER_ID = ""
TEST_OIDC_SERVICE_SECRET = ""
##################################################################
# Classes
##################################################################

from d2wutils.acocl_serv import Serv
class OIDCServ(Serv):
    """
    """

    def get_request_handler(self, path):
        """Handles GET requests. Customise this for the project
        """
        if path == PAGE_INDEX:
            content = self.read_page_file(PAGE_INDEX)
            self.send_html(content)
        elif self.path.startswith(ENDPOINT_PURCHASE_GET):
            self.handle_purchase_get_endpoint(path)
        else:
            self.send_error(
                404,
                HTTP_STATUS_MSG_NOT_FOUND,
                "Could not handle request for {}".format(path[1:]
                )
            )

    def handle_purchase_get_endpoint(self, path):
        """
        """
        _logger.info("handle_purchase_get_endpoint")
            
        endpoint, delim, user_id = path.rpartition('/')
        _logger.info("endpoint %s", endpoint)
        _logger.info("user_id %s", user_id)
        _logger.info("head %s", self.headers)

        #
        # WARNGING! 
        # Multiple exit points from here. 'Goto' returns.
        # We only want to reach the end if all criteara
        # have been met.
        # 
        try:
            content_length = int(self.headers['Content-Length'])
            _logger.info("content_length %s", content_length)
            post_data = self.rfile.read(content_length)
            data = json.loads(post_data.decode('utf-8'))
        except TypeError:
            _logger.info("TypeError on headers Content-Length. Probably no content.")
            self.send_json(JSON_SERVICE_ERROR)
        else:
            if(data['service_secret'] != TEST_OIDC_SERVICE_SECRET):
                _logger.info("service_secret failed")
                self.send_json(JSON_SERVICE_ERROR)
                return;
            _logger.info("service_secret accepted")
            
            if (user_id != TEST_OIDC_USER_ID):
                _logger.info("Unknown user ID %s", user_id)
                self.send_json("[]")
                return;
            _logger.info("User ID accepted")
        
            json_data = self.read_json_file(FILE_SAMPLE_JSON)
            self.send_json(json_data)


    def post_request_handler(self, post_data):
        """Handles POST requests. Customise this for the project
        """
        _logger.info("post_request_handler() OVERRIDE")
        _logger.info("--- SIMULATED UPDATE OIDC ---");
        self.send_json(JSON_SERVICE_OK)


##################################################################
# Utility functions
##################################################################


##################################################################
# Logic for project requirements
##################################################################


##################################################################
# These functions control the major tasks
##################################################################

def main(server_class=HTTPServer, handler_class=OIDCServ, port=DEFAULT_PORT):
    """Process the user's commands.

    Args:
        argv: The command line options.
    """    
    start_time = datetime.now()
    logging_level = logging.INFO
    if _debug:
        logging_level = logging.DEBUG

    try:
        settings_file = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            "settings.yml"
        )
        settings = get_settings(settings_file)
        setup_logging(
            _logger,
            settings['env'],
            logging_level  # <--- Set logging level
            # logging.DEBUG  # <--- Set logging level
            # logging.WARNING  # <--- Set logging level
        )
    except Exception as ex:
        _logger.error("Could not initialise the script")
        raise

    os.chdir('.') # Create the server at the current directory
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    _logger.info('Starting httpd %s...', start_time)
    _logger.info('Connect at http://127.0.0.1:%s/', port)
    _logger.info('Ctrl+C to stop')
    
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        httpd.server_close()
        _logger.info('Stopping httpd...\n')
        _logger.info("Stopped at {}".format(datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
        _logger.info("Server running time: {}".format(datetime.now() - start_time)) 
        if _debug:
            print("*** Script was run in debug mode ***");
    exit(0)

# Program entry point
if __name__ == '__main__':
    from sys import argv

    if len(argv) == 2:
        main(port=int(argv[1]))
    else:
        main()
